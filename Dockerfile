FROM alpine
LABEL maintainer="Mark Bools <saltyvagrant@gmail.com>"

COPY assets/* /opt/resource/

RUN apk --update --no-cache add \
    bash \
    ca-certificates \
    curl \
    jq \
    && chmod +x /opt/resource/*
