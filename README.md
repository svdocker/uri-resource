Provides a `uri-resource` for use by Concourse.

To build:

~~~bash
docker build -t saltyvagrant/uri-resource .
~~~


